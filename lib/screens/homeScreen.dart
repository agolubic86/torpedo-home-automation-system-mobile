import 'package:flutter/material.dart';

import '../appTheme.dart';

import '../models/tabIconData.dart';

import '../widgets/appBottomBar.dart';

import '../screens/roomsScreen.dart';
import '../screens/devicesScreen.dart';
import '../screens/schedulerScreen.dart';
import '../screens/addRoomScreen.dart';
import '../widgets/addScheduledTask.dart';

enum FilterOptions { Favorites, All }

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{

  AnimationController animationController;
  List<TabIconData> tabIconsList = TabIconData.tabIconsList;

  int tabIndex = 0;

  Widget tabBody = Container(
    color: AppTheme.background,
  );

  @override
  void initState() {
    super.initState();

    tabIconsList.forEach((TabIconData tab) {
      tab.isSelected = false;
    });
    tabIconsList[0].isSelected = true;

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    tabBody = RoomsScreen(animationController: animationController);
  }


  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.background,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: FutureBuilder<bool>(
          future: getData(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (!snapshot.hasData) {
              return const SizedBox();
            } else {
              return Stack(
                children: <Widget>[
                  tabBody,
                  bottomBar(),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  void setTab(Widget widget) {
    animationController.reverse().then<dynamic>((data) {
      if (!mounted) {
        return;
      }
      setState(() {
        tabBody = widget;
      });
    });
  }

  Widget bottomBar() {
    return Column(
      children: <Widget>[
        const Expanded(
          child: SizedBox(),
        ),
        AppBottomBar(
          tabIconsList: tabIconsList,
          addClick: () { showModalBottomSheet(
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              context: context,
              builder: (context) {
                if(tabIndex == 2) {
                  return AddScheduledTask();
                }
                return AddRoomScreen();
              }
            );/*Navigator.of(context).pushNamed(AddRoomScreen.routeName);*/
          },
          changeIndex: (int index) {
            tabIndex = index;
            switch(index){
              case 0:
                setTab(RoomsScreen(animationController: animationController));
                break;
              case 1:
                setTab(DevicesScreen(animationController: animationController));
                break;
              case 2:
                setTab(SchedulerScreen(animationController: animationController));
                break;
              case 3:
                setTab(SizedBox.shrink());
                break;
            }
          },
        ),
      ],
    );
  }
}
