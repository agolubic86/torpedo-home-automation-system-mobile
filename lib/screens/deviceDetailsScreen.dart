import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import '../widgets/lineChartView.dart';

import '../appTheme.dart';
import '../helpers/CommonUtils.dart' show Debouncer;

import '../providers/device.dart';
import '../providers/devices.dart';

import '../models/temperature.dart';
import '../models/humidity.dart';


class DeviceDetailsScreen extends StatefulWidget {
  static const routeName = '/device-details';

  @override
  _DeviceDetailsScreenState createState() => _DeviceDetailsScreenState();
}

class _DeviceDetailsScreenState extends State<DeviceDetailsScreen> with TickerProviderStateMixin{

  AnimationController animationController;
  final ScrollController scrollController = ScrollController();
  var deviceNameController;
  var _isInit = true;
  bool isEditName = false;
  Device _device;
  final _debouncer = Debouncer(milliseconds: 500);

  void changeColor(Color color) {
    if(_device.color != color) {
      Provider.of<Device>(context, listen: false).changeColor(color);
    }
  }

  Future<Null> _refreshDevices(BuildContext context) async {
    await Provider.of<Devices>(context, listen: false).findAllDevices();
    _device = Provider.of<Devices>(context, listen: false).findById(_device.id);
  }

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
  }

  @override
  void dispose() {
    animationController.dispose();
    deviceNameController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final deviceId = ModalRoute.of(context).settings.arguments as String;
      if (deviceId != null) {
        _device = Provider.of<Devices>(context, listen: false).findById(deviceId);
      }
    }
    deviceNameController = new TextEditingController(text: _device.name != null ? _device.name : _device.type.toString());
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    animationController.forward();
    return RefreshIndicator(
      onRefresh: () => _refreshDevices(context),
      child: Container(
        child: Scaffold(
          backgroundColor: AppTheme.background,
          appBar: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
            titleSpacing: 0.0,
            title: TextButton.icon(
                icon: Icon(Icons.arrow_back_ios, color: Colors.black),
                label: Text("Back", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                onPressed: () => Navigator.of(context).pop(),
              ),
          ),
          body: SingleChildScrollView(
            controller: scrollController,
            scrollDirection: Axis.vertical,
            child: SizedBox(
              height: deviceSize.height - 80,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 150,
                    right: 35,
                    child: Icon(AppTheme.icon[_device.icon], size: 180, color: AppTheme.flatUiBlue,),
                  ),
                  ChangeNotifierProvider.value(
                    value: _device,
                    child: Consumer<Device>(
                      builder: (ctx, deviceData, child) {
                        return Padding(
                          padding: EdgeInsets.only(top: 50, left: 30, right: 30),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              if (isEditName) Padding(
                                padding: const EdgeInsets.only(right: 16, bottom: 8, top: 16),
                                child: Row(
                                children: <Widget>[
                                    SizedBox(
                                      width: deviceSize.width - 120,
                                      child: TextField(
                                        controller: deviceNameController,
                                        onEditingComplete: () {
                                          deviceData.changeName(deviceNameController.text).then((_) {
                                            setState(() {
                                              isEditName = false;
                                            });
                                          });
                                        },
                                        textInputAction: TextInputAction.done,
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                                          labelText: 'Device name',
                                        ),
                                      ),
                                    ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 15,),
                                        child: InkWell(
                                          onTap: () {
                                            deviceData.changeName(deviceNameController.text).then((_) {
                                            setState(() {
                                              isEditName = false;
                                            });
                                          });
                                          },
                                          child: Icon(Icons.done, size: 25, color: AppTheme.flatUiBlue,)
                                        ),
                                      ),
                                  ]
                                ),
                              ) else Padding(
                                  padding: EdgeInsets.only(bottom: 35),
                                  child: Row(
                                    children: <Widget>[
                                    Expanded(
                                      child: Text(_device.name != null ? _device.name : _device.type.toString(),
                                        style: TextStyle(
                                          color: AppTheme.flatUiBlue,
                                          fontSize: 35,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 35),
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            isEditName = true;
                                          });
                                        },
                                        child: Icon(Icons.edit, size: 25, color: AppTheme.flatUiBlue,)
                                      ),
                                    ),
                                  ],
                                  )
                              ),
                              Text("Device status", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Transform.scale(
                                  scale: 2,
                                  child: Switch(
                                    value: DeviceStatus.ON == _device.status,
                                    onChanged: (status) {
                                      deviceData.changeStatus(status ? DeviceStatus.ON : DeviceStatus.OFF);
                                    },
                                    activeColor: AppTheme.flatUiEmerald,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              (DeviceType.TEMPERATURE != _device.type && DeviceType.SOIL_SENSOR != _device.type && DeviceType.TEMPERATURE != _device.type) ?
                              DeviceType.LIGHT_SWITCH != _device.type ? getColorPickerWidget(context, deviceData) : SizedBox.shrink()
                              :
                              LineChartView(DeviceType.TEMPERATURE == _device.type ? prepareTemperatureDataForChart(_device.temperature) : prepareHumidityDataForChart(_device.humidity)),
                            ],
                          ),
                        );
                      }
                    ),
                  ),
                  /*Positioned(
                    bottom: -100,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 0),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                            child: Container(
                                padding: EdgeInsets.all(20),
                                height: 300,
                                width: deviceSize.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                                  color: AppTheme.white,
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 8,
                                      color: Colors.black26,
                                      offset: Offset(0, 2),
                                    )
                                  ],
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Schedule", style: TextStyle(fontSize: 24, color: AppTheme.flatUiBlue, fontWeight: FontWeight.bold),),
                                    SizedBox(height: 15,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text("From", style: TextStyle(fontSize: 20, color: AppTheme.flatUiBlue, ),),
                                        Text("18:00", style: TextStyle(fontSize: 22, color: AppTheme.flatUiBlue, fontWeight: FontWeight.bold),),
                                        Text("To", style: TextStyle(fontSize: 20, color: AppTheme.flatUiBlue),),
                                        Text("23:30", style: TextStyle(fontSize: 22, color: AppTheme.flatUiBlue, fontWeight: FontWeight.bold),),
                                        Row(
                                          children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(right: 5),
                                            child: Icon(Icons.delete, size: 25, color: AppTheme.flatUiBlue,)
                                          ),
                                          Icon(Icons.edit, size: 25, color: AppTheme.flatUiBlue,),
                                        ],),
                                      ],
                                    ),
                                  ]
                            ),
                          ),
                        ),
                    ),
                  ),*/
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getColorPickerWidget(context, deviceData) {
      return Column(children: <Widget>[
        Text("Device color", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: InkWell(
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    titlePadding: const EdgeInsets.all(0.0),
                    contentPadding: const EdgeInsets.all(0.0),
                    content: SingleChildScrollView(
                      child: SlidePicker(
                        pickerColor: _device.color,
                        onColorChanged: (color) {
                          _debouncer.run(() => deviceData.changeColor(color));
                        },
                        enableAlpha: true,
                        displayThumbColor: true,
                        labelTypes: [],
                        //paletteType: PaletteType.rgb,
                      ),
                    ),
                  );
                },
              );
            },
            child: Container(
              width: 80,
              height: 80,
              decoration: ShapeDecoration(
                shape: CircleBorder(side: BorderSide(color: AppTheme.light_grey, width: 2)),
                color: _device.color,
              ),
            ),
          ),
        )
      ]
    );
  }

  Map<String, dynamic> prepareTemperatureDataForChart(List<Temperature> temperature) {

    double minTemperature = temperature.reduce((curr, next) => curr.value < next.value ? curr : next).value;
    double maxTemperature = temperature.reduce((curr, next) => curr.value > next.value ? curr : next).value;
    List<FlSpot> spots = new List();

    double index = 0;
    for (var item in temperature) {
      spots.add(FlSpot(index, item.value));
      index++;
    }

    Map<String, dynamic> chartDataMap = new Map();
    chartDataMap['minX'] = 0.0.toDouble();
    chartDataMap['maxX'] = temperature.length.toDouble() -1;
    chartDataMap['minY'] = minTemperature - 5;
    chartDataMap['maxY'] = maxTemperature + 5;
    chartDataMap['spots'] = spots;

    return chartDataMap;
  }

  Map<String, dynamic> prepareHumidityDataForChart(List<Humidity> humidity) {

    double minHumidity = 0;
    double maxHumidity = 0;
    if(humidity.isNotEmpty) {
      minHumidity = humidity
          .reduce((curr, next) => curr.value < next.value ? curr : next)
          .value;
      maxHumidity = humidity
          .reduce((curr, next) => curr.value > next.value ? curr : next)
          .value;
    }

    List<FlSpot> spots = new List();

    double index = 0;
    for (var item in humidity) {
      spots.add(FlSpot(index, item.value));
      index++;
    }

    if(spots.isEmpty) {
      spots.add(FlSpot(0, 0));
    }

    Map<String, dynamic> chartDataMap = new Map();
    chartDataMap['minX'] = 0.0.toDouble();
    chartDataMap['maxX'] = humidity.length.toDouble() -1;
    chartDataMap['minY'] = minHumidity > 30 ? minHumidity - 20 : 0.0 ;
    chartDataMap['maxY'] = maxHumidity > 95 ? 100.0 : maxHumidity + 20;
    chartDataMap['spots'] = spots;

    return chartDataMap;
  }
}
