import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../appTheme.dart';

import '../providers/devices.dart';

import '../widgets/appTopBar.dart';
import '../widgets/deviceListView.dart';

class DevicesScreen extends StatefulWidget {

  const DevicesScreen({Key key, this.animationController}) : super(key: key);

  final AnimationController animationController;

  @override
  _DevicesScreenState createState() => _DevicesScreenState();
}

class _DevicesScreenState extends State<DevicesScreen> with TickerProviderStateMixin {

  List<Widget> listViews = <Widget>[];
  final ScrollController scrollController = ScrollController();

  Future<Null> _refreshDevices(BuildContext context) async {
    await Provider.of<Devices>(context, listen: false).findAllDevices();
  }

  @override
  void initState() {
    super.initState();
    addAllListData();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
        height: deviceSize.height - 60,
        color: AppTheme.background,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Stack(
              children: <Widget>[
                //getMainListViewUI(),
                AppTopBar(title: 'Devices', animationController: widget.animationController),
                getMainListViewUI(),
                SizedBox(
                  height: MediaQuery.of(context).padding.bottom,
                )
              ],
            ),
          ),
    );
  }

  Widget getMainListViewUI() {
    return ListView.builder(
      controller: scrollController,
      padding: EdgeInsets.only(
        top: AppBar().preferredSize.height + MediaQuery.of(context).padding.top + 24,
        //bottom: 62 + MediaQuery.of(context).padding.bottom,
      ),
      itemCount: listViews.length,
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        widget.animationController.forward();
        return listViews[index];
      },
    );
  }

  void addAllListData() {
    const int count = 9;

    listViews.add(
      DeviceListView(
        mainScreenAnimation: Tween<double>(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: widget.animationController,
                curve: Interval((1 / count) * 5, 1.0,
                    curve: Curves.fastOutSlowIn))),
        mainScreenAnimationController: widget.animationController,
        showAll: true,
      ),
    );
  }

}
