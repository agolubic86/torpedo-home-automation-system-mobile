import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../appTheme.dart';

import '../providers/appIcons.dart';
import '../providers/appColors.dart';
import '../providers/devices.dart';
import '../providers/device.dart';
import '../providers/room.dart';
import '../providers/rooms.dart';

import '../widgets/colorSelector.dart';
import '../widgets/deviceSelector.dart';
import '../widgets/iconSelector.dart';
import '../widgets/loader.dart';

class AddRoomScreen extends StatefulWidget {
  static const routeName = '/add-room';
  
  @override
  _AddRoomScreenState createState() => _AddRoomScreenState();
}

class _AddRoomScreenState extends State<AddRoomScreen> with TickerProviderStateMixin {

  //animation
  AnimationController animationController;
  Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;

  // input fields
  String selectedRoomType;
  String selectedIcon;
  String selectedColor;
  List<String> selectedDevices = [];
  final roomNameController = TextEditingController();

  var _isInit = true;
  var _isLoading = false;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setAnimationData();
  }


  @override
  void dispose() {
    roomNameController.dispose();
    animationController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    if(_isInit) {
      Provider.of<AppColors>(context).fetchAndSetColors();
      Provider.of<AppIcons>(context).setIcons();
      Provider.of<Devices>(context).findAllDevices();
    }

    setState(() {
      _isInit = false;
    });

    super.didChangeDependencies();
  }

  Future<void> setAnimationData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
  }

  Future<void> addNewRoom() async {
    setState(() {
      _isLoading = true;
    });

    String roomName = roomNameController.text.isEmpty ? Rooms.roomsMap[selectedRoomType] : roomNameController.text;
    Room newRoom = new Room(name: roomName, type: selectedRoomType, color: selectedColor, icon: selectedIcon);
    if(newRoom.isValid()) {
      await Future<dynamic>.delayed(const Duration(milliseconds: 500));
      Provider.of<Rooms>(context, listen: false).addRoom(newRoom).then((_) => {
        setState(() {
          _isLoading = false;
        })
      });
    }
  }

  void onColorSelect(String hexColor) {
    setState(() {
      selectedColor = hexColor;
    });
  }

  void onIconSelect(String icon) {
    setState(() {
      selectedIcon = icon;
    });
  }

  void onDeviceSelect(Device device) {
    setState(() {
      selectedDevices.add(device.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final deviceOrientation = MediaQuery.of(context).orientation;

    return Container(
        child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(width: deviceSize.width,)
                ],
              ),
              Positioned(
                top: 60,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppTheme.nearlyWhite,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(32.0),
                        topRight: Radius.circular(32.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppTheme.grey.withOpacity(0.2),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Stack( children: <Widget>[
                    Positioned(
                      top: 15,
                      left: deviceSize.width / 2 - 60,
                      child: SizedBox(
                        width: 120,
                        height: 7,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4.0)),
                            color: AppTheme.light_grey,

                          ),),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 8, right: 8),
                      child: Container(
                          constraints: BoxConstraints(
                              minHeight: 500,
                              maxHeight: deviceOrientation == Orientation.portrait ? deviceSize.height : deviceSize.width),
                          child: _isLoading ? Center(child: Loader(),) : Stack(
                            children: <Widget>[
                              AnimatedOpacity(
                                duration: Duration(milliseconds: 500),
                                opacity: opacity1,
                                child: Container(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      TextButton(
                                        onPressed: () { Navigator.of(context).pop(); },
                                        child: Text(
                                          'CANCEL',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                            letterSpacing: 0.27,
                                            color: AppTheme.light_grey,
                                          ),
                                        ),),
                                      Text(
                                        'Add Room',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          letterSpacing: 0.27,
                                          color: AppTheme.darkerText,
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () {
                                          addNewRoom().then((_) => {
                                              Navigator.of(context).pop()
                                        });
                                        },
                                        child: Text(
                                          'DONE',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                            letterSpacing: 0.27,
                                            color: AppTheme.flatUiBlue,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),

                              AnimatedOpacity(
                                duration: Duration(milliseconds: 500),
                                opacity: opacity2,
                                child: Container(
                                  margin: EdgeInsets.only(top: 50.0),
                                  child: SingleChildScrollView(
                                    child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[

                                      Divider(
                                        color: AppTheme.light_grey,
                                      ),

                                      getSectionTitle('Enter room name'),
                                      Padding(
                                          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                                          child: TextField(
                                            controller: roomNameController,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(10),
                                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                                              labelText: 'Room name',
                                            ),
                                          ),
                                      ),

                                      getSectionTitle('Choose room type'),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 16, right: 16, bottom: 8, top: 16),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                              border: Border.all(color: AppTheme.light_grey, width: 1, style: BorderStyle.solid)
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                              child: DropdownButton<String>(
                                                isExpanded: true,
                                                value: selectedRoomType,
                                                icon: Icon(Icons.expand_more),
                                                iconSize: 24,
                                                elevation: 16,
                                                style: TextStyle(color: AppTheme.flatUiBlue),
                                                underline: Container(
                                                  color: Colors.transparent,
                                                ),
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    selectedRoomType = newValue;
                                                  });
                                                },
                                                items: Rooms.roomsMap.map((String key, String value) {
                                                  return MapEntry(
                                                      key,
                                                      DropdownMenuItem<String>(
                                                        value: key,
                                                        child: Text(value),
                                                      ));
                                                }).values.toList(),
                                              ),
                                            ),
                                          ),
                                      ),
                                      getSectionTitle('Pick the color'),
                                      ColorSelector(onColorSelect),

                                      getSectionTitle('Pick the icon'),
                                      IconSelector(onIconSelect),

                                      getSectionTitle('Add devices'),
                                      DeviceSelector(onSelect: onDeviceSelect, unregisteredOnly: true),

                                      SizedBox(
                                        height: MediaQuery.of(context).padding.bottom,
                                      )
                                    ],
                            ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                    ),
                    ],
                  ),
                ),
              ),
            ],
          ),
    );
  }

  Widget getSectionTitle(final String title) {
    return Padding(
          padding: const EdgeInsets.only(
              left: 16, right: 16, top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  letterSpacing: 0.27,
                  color: AppTheme.darkerText,
                ),
              ),
            ],
          ),
        );
  }
}
