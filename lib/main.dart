import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import './providers/authProvider.dart';
import './providers/devices.dart';
import './providers/rooms.dart';
import './providers/scheduledTasks.dart';
import './providers/appColors.dart';
import './providers/appIcons.dart';
import './providers/days.dart';

import './screens/splashScreen.dart';
import './screens/authScreen.dart';
import './screens/homeScreen.dart';
import './screens/addRoomScreen.dart';
import 'appConfig.dart';
import 'appTheme.dart';
import 'screens/deviceDetailsScreen.dart';

void main({String env}) async {
  WidgetsFlutterBinding.ensureInitialized();
  final config = await AppConfig.forEnvironment(env);

  GetIt.I.registerSingleton<AppConfig>(config);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: AuthProvider(),
        ),
        ChangeNotifierProxyProvider<AuthProvider, Devices>(
          update: (_, auth, previousProducts) => Devices(
            auth.token,
            auth.userId,
            previousProducts == null ? [] : previousProducts.devices
          ),
        ),
        ChangeNotifierProxyProvider<AuthProvider, Rooms>(
          update: (ctx, auth, previousRooms) => Rooms(auth.token,
              auth.userId, previousRooms == null ? [] : previousRooms.rooms),
        ),
        ChangeNotifierProxyProvider<AuthProvider, ScheduledTasks>(
          update: (ctx, auth, previousScheduledTasks) => ScheduledTasks(auth.token,
              auth.userId, previousScheduledTasks == null ? [] : previousScheduledTasks.scheduledTasks),
        ),
        ChangeNotifierProxyProvider<AuthProvider, AppColors>(
          update: (ctx, auth, previousAppColors) => AppColors( previousAppColors == null ? [] : previousAppColors.colors),
        ),
        ChangeNotifierProxyProvider<AuthProvider, AppIcons>(
          update: (ctx, auth, previousAppIcons) => AppIcons(previousAppIcons == null ? [] : previousAppIcons.icons),
        ),
        ChangeNotifierProxyProvider<AuthProvider, Days>(
          update: (ctx, auth, previousDays) => Days(previousDays == null ? [] : previousDays.days),
        ),
      ],
      child: Consumer<AuthProvider>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Torpedo Home Automation System',
          theme: ThemeData(
            primaryColor: AppTheme.flatUiBlue,
            accentColor: Colors.deepOrange,
            fontFamily: 'Gilroy',
            appBarTheme: AppBarTheme(
              color: AppTheme.background,
            ),
          ),
          home: auth.isAuthorized
              ? HomeScreen()
              : FutureBuilder(
            future: auth.tryAutoLogin(),
            builder: (ctx, authResult) =>
            authResult.connectionState == ConnectionState.waiting
                ? SplashPage()
                : AuthScreen(),
          ),
          routes: {
            AddRoomScreen.routeName: (ctx) => AddRoomScreen(),
            DeviceDetailsScreen.routeName: (ctx) => DeviceDetailsScreen(),
          },
        ),
      ),
    );
  }
}