import 'package:flutter/material.dart';

import '../appTheme.dart';

class Day with ChangeNotifier{

  final String index;
  final String name;
  final String displayName;
  bool isSelected;

  Day({this.index, this.name, this.displayName, this.isSelected = false});

  void toggleIsSelected() {
    isSelected = !isSelected;
    notifyListeners();
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Day &&
          runtimeType == other.runtimeType &&
          index == other.index &&
          name == other.name;

  @override
  int get hashCode => index.hashCode ^ name.hashCode;
}