import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:torpedo_home_automation_system/providers/device.dart';

class ScheduledTask with ChangeNotifier{
  final String id;
  final String name;
  final List<String> days;
  final String startTime;
  final String endTime;
  final Device device;
  bool isActive;

  ScheduledTask({
    @required this.id,
    @required this.name,
    @required this.days,
    @required this.startTime,
    @required this.endTime,
    @required this.device,
    this.isActive = false,
  });

  bool isValid() {
    if(this.name == null)
      return false;

    if(this.days == null || this.days.length == 0)
      return false;

    if(this.startTime == null)
      return false;

    if(this.endTime == null)
      return false;

    if(this.device == null && this.device.id == null)
      return false;

    return true;
  }

}