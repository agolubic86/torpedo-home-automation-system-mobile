import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../appConfig.dart';
import '../helpers/CommonUtils.dart' show Helper ;

import '../models/httpException.dart';
import '../models/temperature.dart';
import '../models/humidity.dart';

import './device.dart';

class Devices with ChangeNotifier {
  List<Device> _devices = [];
  final String authToken;
  final String userId;


  Devices(this.authToken, this.userId, this._devices);

  List<Device> get devices {
    return [
    ..._devices
    ];
  }

  Future<void> updateDevices(List<Device> devices) async{
    _devices = devices;
    notifyListeners();
  }

  List<Device> get unregisteredDevices {

      List<Device> devicesWithoutRoom = [];
      for (var device in _devices) {
        if(device.roomId == 'UNDEFINED' || device.roomId == null) {
          devicesWithoutRoom.add(device);
        }
      }

      return [
      ...devicesWithoutRoom
      ];
  }

  Device findById(String id) {
    return _devices.firstWhere((device) => device.id == id);
  }

  List<Device> findByRoomId(String roomId) {
    return _devices.where((device) => device.roomId == roomId).toList();
  }

  Future<void> findAllDevices() async {

      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/devices/");
      try {
        final response = await http.get(url);
        if(response.statusCode == 404) {
          throw HttpException;
        }
        final extractedData = json.decode(response.body) as List<dynamic>;
        final List<Device> loadedDevices = [];
        if (extractedData == null) {
          return;
        }

        extractedData.forEach((deviceData) {
          loadedDevices.add(
              Device(
                id: deviceData['id'],
                name: deviceData['name'],
                icon: deviceData['icon'],
                color: Helper.getColorFromRgbaMap(deviceData['color']),
                status: getDeviceStatusByName(deviceData['status']),
                type: getDeviceTypeByName(deviceData['type']),
                temperature: Temperature.mapTemperature(deviceData['temperature']),
                humidity: Humidity.mapHumidity(deviceData['humidity']),
                roomId: deviceData['roomId'],
              ));
        });
        _devices = loadedDevices;
        notifyListeners();
      } catch (error) {
        throw error;
      }
  }

  /*Future<void> updateDevice(String id, Device newDevice) async{
    final productIndex = _items.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      try {
        final url = new Uri.http("192.168.0.16:2225", "/devices/$id");
        await http.put(url, body: json.encode({
          'title': newProduct.name,
          'description': newProduct.description,
          'imageUrl': newProduct.imageUrl,
          'price': newProduct.price
        }));
      }catch(error) {
        print(error);
      }
      _items[productIndex] = newProduct;
      notifyListeners();
    } else {
      print('...');
    }
  }*/

  /*Future<void> findDevicesByRoom({int roomId}) async {

    if(roomId != null) {
      final url = new Uri.http("192.168.0.16:2225", "/device/room/$roomId");
      try {
        final response = await http.get(url);
        print('#### FIND DEVICES BY ROOM ######');
        print('#### Print response ######');
        print(response.statusCode);
        final extractedData = json.decode(response.body) as List<dynamic>;
        final List<Device> loadedDevices = [];
        if (extractedData == null) {
          return;
        }

        extractedData.forEach((deviceData) {
          loadedDevices.add(
              Device(
                id: deviceData['id'],
                name: deviceData['name'],
                icon: deviceData['icon'],
                status: getDeviceStatusByName(deviceData['status']),
                room: deviceData['room'],
              ));
        });
        _devices = loadedDevices;
        notifyListeners();
      } catch (error) {
        throw error;
      }
    } else {
      _devices = [];
    }
  }*/

  /*Future<void> addProduct(DeviceProvider product) async {
    final url = 'https://flutter-demo-94c0a.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.post(url, body: json.encode({
        'title': product.name,
        'description': product.description,
        'imageUrl': product.imageUrl,
        'price': product.price,
        'userId': userId,
      }),);

      final newProduct = new DeviceProvider(
          id: json.decode(response.body)['id'],
          name: product.name,
          description: product.description,
          price: product.price,
          imageUrl: product.imageUrl);

      _items.add(newProduct);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }*/

  /*Future<void> updateProduct(String id, DeviceProvider newProduct) async{
    final productIndex = _items.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      try {
        final url = 'https://flutter-demo-94c0a.firebaseio.com/products/$id.json?auth=$authToken';
        await http.patch(url, body: json.encode({
          'title': newProduct.name,
          'description': newProduct.description,
          'imageUrl': newProduct.imageUrl,
          'price': newProduct.price
        }));
      }catch(error) {
        print(error);
      }
      _items[productIndex] = newProduct;
      notifyListeners();
    } else {
      print('...');
    }
  }*/

 /* Future<void> deleteProduct(String id) async {
    final url = 'https://flutter-demo-94c0a.firebaseio.com/products/$id.json?auth=$authToken';
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    var existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    notifyListeners();
    final response = await http.delete(url);
    if(response.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Could not delete product.');
    }
    existingProduct = null;
  }*/


}