import 'package:flutter/material.dart';

class AppIcon with ChangeNotifier {
  final String name;
  final Icon icon;
  bool isSelected;

  AppIcon({@required this.name, @required this.icon, this.isSelected = false});

  void toggleIsSelected() {
    isSelected = !isSelected;
    notifyListeners();
  }

}