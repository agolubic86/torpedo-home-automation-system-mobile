
import 'package:flutter/material.dart';

import './day.dart';
class Days with ChangeNotifier {
  List<Day> _days = [];
  static List<Day> _dayz = [];

  Days(this._days);

  Future<void> setDays() async {
    _days = [
      new Day(index: "0", name: "SUNDAY", displayName: "S", isSelected: false),
      new Day(index: "1", name: "MONDAY", displayName: "M", isSelected: false),
      new Day(index: "2", name: "TUESDAY", displayName: "T", isSelected: false),
      new Day(index: "3", name: "WEDNESDAY", displayName: "W", isSelected: false),
      new Day(index: "4", name: "THURSDAY", displayName: "T", isSelected: false),
      new Day(index: "5", name: "FRIDAY", displayName: "F", isSelected: false),
      new Day(index: "6", name: "SATURDAY", displayName: "S", isSelected: false)
    ];

    _dayz = [..._days];
  }

  Future<void> updateDays(List<Day> days) async {
    _days = days;
    notifyListeners();
  }

  static Day getByName(String dayName) {
    return _dayz.firstWhere((day) => day.name == dayName);
  }

  static Day getByIndex(String dayIndex) {
    return _dayz.firstWhere((day) => day.index == dayIndex);
  }

  List<Day> get days {
    return[..._days];
  }
}