import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../appConfig.dart';
import '../models/httpException.dart';
import './room.dart';

class Rooms with ChangeNotifier {
  List<Room> _rooms = [];
  final String authToken;
  final String userId;

  Rooms(this.authToken, this.userId, this._rooms);

  static Map<String, String> roomsMap = {
    "LIVING_ROOM":"Living room",
    "BEDROOM": "Bedroom",
    "KITCHEN": "Kitchen",
    "LOGGIA": "Loggia",
    "PANTRY": "Pantry",
    "MAIN_HALL": "Main hall",
    "HALL": "Hall",
    "BATHROOM": "Bathroom",
    "TOILET": "Toilet",
    "WORK_ROOM": "Work room"
  };

  List<Room> get rooms {
    return [..._rooms];
  }

  Future<void> getAndSetRooms() async {
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/rooms");

      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List<dynamic>;
      if (extractedData == null) {
        return;
      }

      final List<Room> loadedRooms = [];
      int index = 0;
      extractedData.forEach((roomData) {
        loadedRooms.add(
          Room(
              id: roomData['id'],
              name: roomData['name'],
              color: roomData['color'],
              icon: roomData["icon"],
              type: roomData['type'],
              temperature: roomData['temperature'],
              isSelected: index == 0
          ),
        );

        index++;
      });

      _rooms = loadedRooms.toList();
      notifyListeners();
    }catch(error) {
      print('###### ERROR fetching rooms #############');
      print(error);
      throw error;
    }
  }

  Future<void> updateRooms(List<Room> rooms) async{
    _rooms = rooms;
    notifyListeners();
  }

 Future<void> addRoom(Room room) async {
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/rooms/name/${room.name}/roomType/${room.type}/color/${room.color}/icon/${room.icon}");
      final response = await http.post(url, body: json.encode({}),);

      final newRoom = new Room(
          id: json.decode(response.body)['id'],
          name: room.name,
          type: room.type,
          color: room.color,
          icon: room.icon);

      _rooms.add(newRoom);
      _rooms.first.isSelected = true;

      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> deleteRoom(String id) async {
    final AppConfig appConfig = GetIt.instance<AppConfig>();
    final url = new Uri.http(appConfig.apiUrl, "/rooms/$id");
    final existingRoomIndex = _rooms.indexWhere((room) => room.id == id);

    var existingRoom = _rooms[existingRoomIndex];
    _rooms.removeAt(existingRoomIndex);
    notifyListeners();
    final response = await http.delete(url);
    if(response.statusCode >= 400) {
      _rooms.insert(existingRoomIndex, existingRoom);
      notifyListeners();
      throw HttpException('Could not delete room.');
    }
    existingRoom = null;
  }

}
