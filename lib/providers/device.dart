import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../appConfig.dart';

import '../models/httpException.dart';
import '../models/temperature.dart';
import '../models/humidity.dart';

enum DeviceStatus {
  ON,
  OFF,
  OPEN,
  CLOSE,
}
enum DeviceType {
  LED,
  LIGHT_SWITCH,
  TEMPERATURE,
  CURTAIN,
  SOIL_SENSOR,
}


DeviceStatus getDeviceStatusByName(String statusName) {
  for (var deviceStatus in DeviceStatus.values) {
    if(describeEnum(deviceStatus) == statusName) {
      return deviceStatus;
    }
  }

  throw Exception("Wrong device status name: $statusName");
}

DeviceType getDeviceTypeByName(String typeName) {
  for (var deviceType in DeviceType.values) {
    if(describeEnum(deviceType) == typeName) {
      return deviceType;
    }
  }

  throw Exception("Wrong device status name: $typeName");
}

class Device with ChangeNotifier{
  final String id;
  final String icon;
  final String roomId;
  final DeviceType type;
  final List<Temperature> temperature;
  final List<Humidity> humidity;
  String name;
  Color color;
  DeviceStatus status;
  bool isSelected;

  Device({
    @required this.id,
    @required this.name,
    @required this.icon,
    @required this.roomId,
    @required this.type,
    this.temperature,
    this.humidity,
    this.color,
    this.status = DeviceStatus.OFF,
    this.isSelected = false,
  });

  Map toMap() {
    return {
      "id": id,
      "name": name,
      "icon": icon,
      "roomId": roomId,
      "type": type.name,
      "status": status.name,
    };
  }

  String get lastHumidityValue {
    if(this.humidity.isEmpty) {
      return "N/A";
    }

    Humidity lastHumidityRead = this.humidity.reduce((curr, next) => curr.date.isAfter(next.date) ? curr : next);
    return lastHumidityRead.value.toString() + "%";
  }

 /* Future<void> changeStatus(DeviceStatus status) async{
      try {
        final AppConfig appConfig = GetIt.instance<AppConfig>();
        Map<String, String> params = {"action": describeEnum(status)};
        final url = new Uri.http(appConfig.apiUrl, "devices/$id", params);
        await http.put(url);

        this.status = status;
        notifyListeners();
      }catch(error) {
        print(error);
      }
    }*/

  Future<void> changeStatus(DeviceStatus status) async{
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      Map<String, String> headers = {"Content-type": "application/json"};
      final url = new Uri.http(appConfig.apiUrl, "devices/$id");
      final response = await http.patch(url, headers: headers,
          body: json.encode([{
            "op": "replace",
            "path": "/status",
            "value": describeEnum(status)
          }])
      );

      if(response.statusCode != 200) {
        throw HttpException;
      }

      this.status = status;
      notifyListeners();
    }catch(error) {
      print(error);
    }
  }

  Future<void> changeColor(Color color) async{
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      Map<String, String> headers = {"Content-type": "application/json"};
      final url = new Uri.http(appConfig.apiUrl, "devices/$id");
      final response = await http.patch(url, headers: headers,
          body: json.encode([{
            "op": "replace",
            "path": "/color",
            "value": {
              "r": color.red,
              "g": color.green,
              "b": color.blue,
              "alpha": 1
            }
          }])
      );

      if(response.statusCode != 200) {
        throw HttpException;
      }

      this.color = color;
      notifyListeners();
    }catch(error) {
      print(error);
    }
  }

  Future<void> changeName(String name) async{
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      Map<String, String> headers = {"Content-type": "application/json"};
      final url = new Uri.http(appConfig.apiUrl, "devices/$id");
      final response = await http.patch(url, headers: headers,
          body: json.encode([{
            "op": "replace",
            "path": "/name",
            "value": name
          }])
      );

      if(response.statusCode != 200) {
        throw HttpException;
      }

      this.name = name;
      notifyListeners();
    }catch(error) {
      print(error);
    }
  }

  Future<void> refreshTemperatureAndHumidity(String id, String temperatureFilterType, String humidityFilterType) async{
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();

      var queryParams = {};
      if(temperatureFilterType != null) {
        queryParams["temperatureFilterType"] = temperatureFilterType;
      }
      if(humidityFilterType != null) {
        queryParams["humidityFilterType"] = humidityFilterType;
      }

      final url = new Uri.http(appConfig.apiUrl, "devices/$id", queryParams);
      final response = await http.get(url);

      if(response.statusCode != 200) {
        throw HttpException;
      }

      final extractedData = json.decode(response.body) as List<dynamic>;
      if (extractedData == null) {
        return;
      }

      notifyListeners();
    }catch(error) {
      print(error);
    }
  }

}