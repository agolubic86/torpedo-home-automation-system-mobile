import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../appConfig.dart';
import './appColor.dart';

class AppColors with ChangeNotifier {
  List<AppColor> _colors = [];

  AppColors(this._colors);

  List<AppColor> get colors {
    return [..._colors];
  }

  Future<void> fetchAndSetColors() async {
    print("Loading colors");

    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/color");
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List<dynamic>;
      final List<AppColor> loadedColors = [];
      if (extractedData == null) {
        return;
      }

      extractedData.forEach((colorData) {
        loadedColors.add(
            AppColor(
              name: colorData['name'],
              colorHex: colorData['colorHex'],
            ));
      });
      _colors = loadedColors;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateColors(List<AppColor> colors) async{
    _colors = colors;
    notifyListeners();
  }

}