import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../appConfig.dart';
import '../models/httpException.dart';

import '../providers/device.dart';
import '../providers/scheduledTask.dart';
import '../providers/days.dart';

class ScheduledTasks with ChangeNotifier {
  List<ScheduledTask> _scheduledTasks = [];
  final String authToken;
  final String userId;

  ScheduledTasks(this.authToken, this.userId, this._scheduledTasks);

  List<ScheduledTask> get scheduledTasks {
    return [..._scheduledTasks];
  }

  Future<void> getAndSetScheduledTasks() async {
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/scheduler");

      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List<dynamic>;
      if (extractedData == null) {
        return;
      }

      final List<ScheduledTask> loadedScheduledTasks = [];
      int index = 0;
      extractedData.forEach((scheduledTaskData) {

        Device device = new Device(
            id: scheduledTaskData['device']['id'],
            name: scheduledTaskData['device']['name'],
            icon: scheduledTaskData['device']['icon'],
            roomId: scheduledTaskData['device']['roomId'],
            type: getDeviceTypeByName(scheduledTaskData['device']['type'])
        );


        loadedScheduledTasks.add(
          ScheduledTask(
              id: scheduledTaskData['id'],
              name: scheduledTaskData['name'],
              device: device,
              days: scheduledTaskData['days'].cast<String>(),
              startTime: scheduledTaskData['startTime'],
              endTime: scheduledTaskData['endTime'],
              isActive: scheduledTaskData['active'],
          ),
        );

        index++;
      });

      _scheduledTasks = loadedScheduledTasks.toList();
      notifyListeners();
    }catch(error) {
      print('###### ERROR fetching scheduler actions #############');
      print(error);
      throw error;
    }
  }

  Future<void> updateScheduledTasks(List<ScheduledTask> scheduledTasks) async{
    _scheduledTasks = scheduledTasks;
    notifyListeners();
  }

 Future<void> addScheduledTask(ScheduledTask scheduledTask) async {
    try {
      final AppConfig appConfig = GetIt.instance<AppConfig>();
      final url = new Uri.http(appConfig.apiUrl, "/scheduler");
      final response = await http.post(url, body: json.encode({
            'name': scheduledTask.name,
            'device': scheduledTask.device.toMap(),
            'days': scheduledTask.days,
            'startTime': scheduledTask.startTime,
            'endTime': scheduledTask.endTime,
          }
        ),
       headers: {
        "content-type" : "application/json",
        "accept" : "application/json",
        },
      );

      final newScheduledTask = new ScheduledTask(
          id: json.decode(response.body)['id'],
          name: scheduledTask.name,
          device: scheduledTask.device,
          days: scheduledTask.days.map((dayIndex) => Days.getByIndex(dayIndex).name).toList(),
          startTime: scheduledTask.startTime,
          endTime: scheduledTask.endTime,
          isActive: scheduledTask.isActive
      );

      _scheduledTasks.add(newScheduledTask);

      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> deleteScheduledTask(String name) async {
    final AppConfig appConfig = GetIt.instance<AppConfig>();
    final url = new Uri.http(appConfig.apiUrl, "/scheduler/$name");
    final existingScheduledTaskIndex = _scheduledTasks.indexWhere((scheduledTask) => scheduledTask.name == name);

    var existingScheduledTask = _scheduledTasks[existingScheduledTaskIndex];
    _scheduledTasks.removeAt(existingScheduledTaskIndex);
    notifyListeners();
    final response = await http.delete(url);
    if(response.statusCode >= 400) {
      _scheduledTasks.insert(existingScheduledTaskIndex, existingScheduledTask);
      notifyListeners();
      throw HttpException('Could not delete scheduler action.');
    }
    existingScheduledTask = null;
  }

}
