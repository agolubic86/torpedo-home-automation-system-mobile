import 'package:flutter/material.dart';

import '../appTheme.dart';
import './appIcon.dart';

class AppIcons with ChangeNotifier{
  List<AppIcon> _icons = [];

  AppIcons(this._icons);

  Future<void> setIcons() async {
    print("loading icons");

    _icons = [
      new AppIcon(name: "sofa", icon: Icon(AppTheme.sofa, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "sink", icon: Icon(AppTheme.sink, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "fridge", icon: Icon(AppTheme.fridge, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "bed", icon: Icon(AppTheme.bed, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "crib", icon: Icon(AppTheme.crib, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "workbench", icon: Icon(AppTheme.workbench, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "toilet", icon: Icon(AppTheme.toilet, color: AppTheme.flatUiBlue), isSelected: false),
      new AppIcon(name: "hallway", icon: Icon(AppTheme.hallway, color: AppTheme.flatUiBlue), isSelected: false),
    ];

  }

  Future<void> updateIcons(List<AppIcon> icons) async{
    _icons = icons;
    notifyListeners();
  }

  List<AppIcon> get icons {
    return [..._icons];
  }
}