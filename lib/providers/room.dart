import 'package:flutter/material.dart';

class Room with ChangeNotifier{
  final String id;
  final String name;
  final String color;
  final String icon;
  final String type;
  final double temperature;
  bool isSelected;

  Room({
    @required this.id,
    @required this.name,
    @required this.color,
    @required this.icon,
    @required this.type,
    @required this.temperature,
    this.isSelected = false
  });

  void toggleIsSelected() {
    isSelected = !isSelected;
    notifyListeners();
  }

  bool isValid() {
    if(this.type == null)
      return false;

    if(this.color == null)
      return false;

    if(this.icon == null)
      return false;

    return true;
  }
}