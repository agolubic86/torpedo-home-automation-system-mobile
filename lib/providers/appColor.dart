import 'package:flutter/material.dart';

class AppColor with ChangeNotifier {
  final String name;
  final String colorHex;
  bool isSelected;

  AppColor({ @required this.name, @required this.colorHex, this.isSelected = false});

  void toggleIsSelected() {
    isSelected = !isSelected;
    notifyListeners();
  }

}