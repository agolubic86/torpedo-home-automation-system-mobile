import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    if(hexColor != null) {
      hexColor = hexColor.toUpperCase().replaceAll('#', '');
      if (hexColor.length == 6) {
        hexColor = 'FF' + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    return int.parse("FFFFFF", radix: 16);
  }
}

class Helper {

  static Color getColorFromRgbaMap(LinkedHashMap colorMap) {
    if(colorMap == null) {
      return Colors.black;
    }

    Color color;
    try {
      color = Color.fromRGBO(colorMap['r'], colorMap['g'], colorMap['b'], 1.0);
    }catch(error) {
      print(error);
      return color = Colors.black;
    }

    return color;
  }
}


class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({ this.milliseconds });

  run(VoidCallback action) {
    if (_timer != null) {
      _timer.cancel();
    }

    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}