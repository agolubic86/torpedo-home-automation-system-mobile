import 'package:flutter/material.dart';

extension TimeOfDayConverter on TimeOfDay {
  String to24hours() {
    final hour = get24Hour();
    final min = get24Minute();
    return "$hour:$min";
  }

  String get24Hour() {
    return this.hour.toString().padLeft(2, "0");
  }

  String get24Minute() {
    return this.minute.toString().padLeft(2, "0");
  }
}