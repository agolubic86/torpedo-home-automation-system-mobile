import 'package:flutter/material.dart';

class TabIconData {
  TabIconData({
    this.imagePath = '',
    this.index = 0,
    this.selectedImagePath = '',
    this.isSelected = false,
    this.animationController,
  });

  String imagePath;
  String selectedImagePath;
  bool isSelected;
  int index;

  AnimationController animationController;

  static List<TabIconData> tabIconsList = <TabIconData>[
    TabIconData(
      imagePath: 'assets/tabs/rooms.png',
      selectedImagePath: 'assets/tabs/rooms_active.png',
      index: 0,
      isSelected: true,
      animationController: null,
    ),
    TabIconData(
      imagePath: 'assets/tabs/devices.png',
      selectedImagePath: 'assets/tabs/devices_active.png',
      index: 1,
      isSelected: false,
      animationController: null,
    ),
    TabIconData(
      imagePath: 'assets/tabs/scheduler.png',
      selectedImagePath: 'assets/tabs/scheduler_active.png',
      index: 2,
      isSelected: false,
      animationController: null,
    ),
    TabIconData(
      imagePath: 'assets/tabs/settings.png',
      selectedImagePath: 'assets/tabs/settings_active.png',
      index: 3,
      isSelected: false,
      animationController: null,
    ),
  ];
}
