import 'package:flutter/material.dart';

import '../appTheme.dart';

class IconItem {

  Map<String, IconData> get _icons {
    return icons;
  }

  static Map<String, IconData> icons = {
    "led": AppTheme.led,
    "temperature": AppTheme.temperature,
    "humidity": AppTheme.humidity,
    "light": AppTheme.lightbulb,
  };

}