
class Humidity {
  final DateTime date;
  final double value;

  Humidity({this.date, this.value});

  static List<Humidity> mapHumidity(List<dynamic> humidityFromResponse) {
    if(humidityFromResponse != null && humidityFromResponse.length > 0) {
      return humidityFromResponse.map((humidity) => Humidity(date: DateTime.fromMillisecondsSinceEpoch(humidity['date']), value: humidity['value'])).toList();
    }

    return [];
  }
}
