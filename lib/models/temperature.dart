
class Temperature {
  final DateTime date;
  final double value;

  Temperature({this.date, this.value});

  static List<Temperature> mapTemperature(List<dynamic> temperaturesFromResponse) {
    if(temperaturesFromResponse != null && temperaturesFromResponse.length > 0) {
      return temperaturesFromResponse.map((temperature) => Temperature(date: DateTime.fromMillisecondsSinceEpoch(temperature['date']), value: temperature['value'])).toList();
    }

    return [];
  }
}
