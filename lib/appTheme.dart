import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  //custom colors
  static const Color nearlyWhite = Color(0xFFFAFAFA);
  static const Color white = Color(0xFFFFFFFF);
  static const Color background = Color(0xFFF2F3F8);
  static const Color nearlyDarkBlue = Color(0xFF2633C5);

  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color light_grey = Color(0xFFbdc3c7);
  static const Color dark_grey = Color(0xFF313A44);

  static const Color flatUiBlue = Color(0xFF2980B9);
  static const Color flatUiEmerald = Color(0xFF2ECC71);
  static const Color flatUiAmethyst = Color(0xFF9b59b6);
  static const Color flatUiCarrot = Color(0xFFe67e22);
  static const Color flatUiRed = Color(0xFFe74c3c);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color spacer = Color(0xFFF2F2F2);
  static const String fontName = 'Roboto';

  // custom icons
  static const _kFontFam = 'TorpedoHome';

  static const IconData toilet = IconData(0xe800, fontFamily: _kFontFam);
  static const IconData hallway = IconData(0xe801, fontFamily: _kFontFam);
  static const IconData fridge = IconData(0xe802, fontFamily: _kFontFam);
  static const IconData sofa = IconData(0xe803, fontFamily: _kFontFam);
  static const IconData clouds_flash_alt = IconData(0xe804, fontFamily: _kFontFam);
  static const IconData sun = IconData(0xe805, fontFamily: _kFontFam);
  static const IconData cloud_sun = IconData(0xe806, fontFamily: _kFontFam);
  static const IconData cloud_moon = IconData(0xe807, fontFamily: _kFontFam);
  static const IconData fog_sun = IconData(0xe808, fontFamily: _kFontFam);
  static const IconData fog_cloud = IconData(0xe809, fontFamily: _kFontFam);
  static const IconData cloud = IconData(0xe80a, fontFamily: _kFontFam);
  static const IconData cloud_flash = IconData(0xe80b, fontFamily: _kFontFam);
  static const IconData drizzle = IconData(0xe80c, fontFamily: _kFontFam);
  static const IconData rain = IconData(0xe80d, fontFamily: _kFontFam);
  static const IconData snow_alt = IconData(0xe80e, fontFamily: _kFontFam);
  static const IconData snow_heavy = IconData(0xe80f, fontFamily: _kFontFam);
  static const IconData hail = IconData(0xe810, fontFamily: _kFontFam);
  static const IconData clouds = IconData(0xe811, fontFamily: _kFontFam);
  static const IconData temperature = IconData(0xe812, fontFamily: _kFontFam);
  static const IconData led = IconData(0xe813, fontFamily: _kFontFam);
  static const IconData lightbulb = IconData(0xe814, fontFamily: _kFontFam);
  static const IconData lock = IconData(0xe815, fontFamily: _kFontFam);
  static const IconData humidity = IconData(0xe816, fontFamily: _kFontFam);
  static const IconData crib = IconData(0xe817, fontFamily: _kFontFam);
  static const IconData bathtub = IconData(0xe818, fontFamily: _kFontFam);
  static const IconData workbench = IconData(0xe819, fontFamily: _kFontFam);
  static const IconData bed = IconData(0xe81a, fontFamily: _kFontFam);
  static const IconData sink = IconData(0xe81b, fontFamily: _kFontFam);

  static const Map<String, IconData> icon = {
    "sink": sink,
    "bed": bed,
    "fridge" : fridge,
    "sofa": sofa,
    "workbench": workbench,
    "bathtub": bathtub,
    "crib": crib,
    "toilet": toilet,
    "hallway": hallway,
    "temperature": temperature,
    "humidity": humidity,
    "led": led,
    "lightbulb": lightbulb,
  };

/*  static const TextTheme textTheme = TextTheme(
    display1: display1,
    headline: headline,
    title: title,
    subtitle: subtitle,
    body2: body2,
    body1: body1,
    caption: caption,
  );*/

  static const TextStyle display1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle title = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle subtitle = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle body2 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle body1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: darkText,
  );

  static const TextStyle caption = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );
}
