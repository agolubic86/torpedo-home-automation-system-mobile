import 'package:flutter/material.dart';
import 'dart:math' as math;

import '../appTheme.dart';

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Animation<double> animationRotation;
  Animation<double> animationRadiusIn;
  Animation<double> animationRadiusOut;

  final double initRadius = 30.0;

  double radius = 0.0;


  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    animationController = AnimationController(vsync: this, duration: Duration(seconds: 5));

    animationRotation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0.0, 1.0, curve: Curves.linear)));

    animationRadiusIn = Tween<double>(
      begin: 1.0,
      end: 0.0,
    ).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0.75, 1.0, curve: Curves.elasticIn)));

    animationRadiusOut = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0.0, 0.25, curve: Curves.elasticOut)));


    animationController.addListener(() {
        setState(() {
        if(animationController.value >= 0.75 && animationController.value <= 1.0) {
          radius = animationRadiusIn.value * initRadius;
        } else if(animationController.value >= 0.0 && animationController.value <= 0.25) {
          radius = animationRadiusOut.value * initRadius;
        }
      });
    });

    animationController.repeat();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      child: RotationTransition(
        turns: animationRotation,
        child: Center(
          child: Stack(
            children: List.generate(9, (i) {
              if(i == 0) {
                return Dot(
                  radius: 30,
                  color: AppTheme.flatUiEmerald.withOpacity(0.5),
                );
              }
              else {
                return Transform.translate(
                  offset: Offset(radius * math.cos(i * math.pi/4), radius * math.sin(i* math.pi/4)),
                  child: Dot(
                    radius: 5,
                    color: AppTheme.flatUiEmerald,
                  ),
                );
              }
            })
          ),
        ),
      ),
    );
  }
}

class Dot extends StatelessWidget {
  final double radius;
  final Color color;


  Dot({this.radius, this.color});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: this.radius,
        decoration: BoxDecoration(
          color: this.color,
          shape: BoxShape.circle
        ),
      ),
    );
  }
}

