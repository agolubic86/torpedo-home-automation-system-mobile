import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../appTheme.dart';

import '../models/iconItem.dart';

import '../providers/devices.dart';


class DeviceSelector extends StatelessWidget {
  final Function onSelect;
  final bool unregisteredOnly;

  DeviceSelector({this.onSelect, this.unregisteredOnly=false});

  @override
  Widget build(BuildContext context) {
    final devicesData = Provider.of<Devices>(context);
    final devices = unregisteredOnly ? devicesData.unregisteredDevices : devicesData.devices;

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Wrap(
          children: devices.map((device) {
            return Stack(
              //overflow: Overflow.visible,
              clipBehavior: Clip.none,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: ChoiceChip(
                      backgroundColor: AppTheme.nearlyWhite,
                      selectedColor: AppTheme.nearlyWhite,
                      avatar: Icon(AppTheme.icon[device.icon], size: 30, color: AppTheme.flatUiBlue),
                      labelPadding: const EdgeInsets.all(5),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10), side: BorderSide(color: AppTheme.light_grey)),
                      label: device.name != null ? Text(device.name, style: TextStyle(color: AppTheme.flatUiBlue),) : Text(device.type.toString(), style: TextStyle(color: AppTheme.flatUiBlue),),
                      selected: device.isSelected,
                      onSelected: (_) {
                        onSelect(device);
                        device.isSelected = !device.isSelected;
                      },
                  ),
                ),
                if (device.isSelected)
                    Positioned(
                      top: -3,
                      right: 3,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppTheme.white),
                        child: Container(
                          child: Icon(
                            Icons.check_circle_outline,
                            color: AppTheme.flatUiEmerald,
                            size: 20,
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                    ),
              ],
            );
          }).toList()),
    );
  }
}
