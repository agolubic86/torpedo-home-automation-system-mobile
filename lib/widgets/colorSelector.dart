import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import '../appTheme.dart';

import '../helpers/CommonUtils.dart';

import '../providers/appColors.dart';
import '../providers/appColor.dart';

class ColorSelector extends StatefulWidget {
  final Function onSelect;


  ColorSelector(this.onSelect);

  @override
  _ColorSelectorState createState() => _ColorSelectorState();
}

class _ColorSelectorState extends State<ColorSelector> {

  Color currentColor = Colors.black;
  bool isColorChanged = false;

  void changeColor(Color color) {
    if(currentColor != color) {
      setState(() {
        currentColor = color;
        isColorChanged = true;
      });
    }
  }

  void setRemoveAllSelection(BuildContext context,List<AppColor> appColors, String selectedColor) {
    appColors.forEach((AppColor color) {
      color.isSelected = false;
      if (selectedColor == color.colorHex) {
        color.isSelected = true;
      }
    });

    Provider.of<AppColors>(context, listen: false).updateColors(appColors);
  }

  @override
  Widget build(BuildContext context) {
    final appColorsData = Provider.of<AppColors>(context);
    final appColors = appColorsData.colors;

    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0),
      child: Container(
        height: 70,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: getColorWidgets(context, appColors)
          ),
        ),
      ),
    );
  }

  List<Widget> getColorWidgets(BuildContext context, List<AppColor> appColors) {
    List<Widget> colors = appColors.map((color) {
          return InkWell(
            onTap: () {
              widget.onSelect(color.colorHex);
              setRemoveAllSelection(context, appColors, color.colorHex);
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Stack(
                //overflow: Overflow.visible,
                clipBehavior: Clip.none,
                children: <Widget>[
                  SizedBox(
                    width: 60,
                    height: 60,
                    child: Container(
                      decoration: BoxDecoration(
                        color: HexColor(color.colorHex),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                    ),
                  ),
                  if (color.isSelected)
                    Positioned(
                      top: -5,
                      right: -5,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppTheme.white),
                        child: Container(
                          child: Icon(
                            Icons.check_circle_outline,
                            color: AppTheme.flatUiEmerald,
                            size: 20,
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          );
      }).toList();

    colors.add(
      InkWell(
        onTap: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                titlePadding: const EdgeInsets.all(0.0),
                contentPadding: const EdgeInsets.all(0.0),
                content: SingleChildScrollView(
                  child: SlidePicker(
                    pickerColor: currentColor,
                    onColorChanged: changeColor,
                    enableAlpha: false,
                    displayThumbColor: true,
                    showLabel: false,
                    //paletteType: PaletteType.rgb,
                  ),
                ),
              );
            },
          ).then((_) {
            if(isColorChanged) {
              List<AppColor> appColors = Provider.of<AppColors>(context).colors;
              appColors.add(new AppColor(name: '#${currentColor.value.toRadixString(16)}', colorHex: '#${currentColor.value.toRadixString(16)}'));
              Provider.of<AppColors>(context, listen: false).updateColors(appColors);
            }
          });
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: Stack(
            //overflow: Overflow.visible,
            clipBehavior: Clip.none,
            children: <Widget>[
              SizedBox(
                width: 60,
                height: 60,
                child: Container(
                  child: Icon(Icons.add, color: AppTheme.flatUiBlue, size: 30,),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppTheme.light_grey),
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                ),
              ),
            ],
          ),
        ),
        )
    );

    return colors;
  }
}
