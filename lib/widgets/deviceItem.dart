import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../appTheme.dart';

import '../providers/device.dart';

import '../screens/deviceDetailsScreen.dart';

class DeviceItem extends StatefulWidget {
  
  const DeviceItem({
    Key key,
    this.animationController,
    this.animation,
    this.color
  }) : super(key: key);

  final AnimationController animationController;
  final Animation<dynamic> animation;
  final Color color;

  @override
  _DeviceItemState createState() => _DeviceItemState();
}

class _DeviceItemState extends State<DeviceItem> {

  bool _showOptions = false;

  @override
  Widget build(BuildContext context) {
    final device = Provider.of<Device>(context);

    return device == null ? SizedBox() : AnimatedBuilder(
      animation: widget.animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - widget.animation.value), 0.0),
            child: Container(
              decoration: BoxDecoration(
                color: AppTheme.white,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppTheme.grey.withOpacity(0.4),
                      offset: const Offset(1.1, 1.1),
                      blurRadius: 10.0),
                ],
              ),
              child: Material(
                color: Colors.transparent,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: <Widget>[
                      InkWell(
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                        splashColor: AppTheme.nearlyDarkBlue.withOpacity(0.2),
                        onLongPress: () {
                          Navigator.of(context).pushNamed(
                            DeviceDetailsScreen.routeName,
                            arguments: device.id,
                          );
                        },
                        onTap: () {
                          Provider.of<Device>(context, listen: false).changeStatus(DeviceStatus.OFF == device.status ? DeviceStatus.ON : DeviceStatus.OFF);
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 20, left: 16, right: 16),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Icon(AppTheme.icon[device.icon], size: 30, color: widget.color),
                                    DeviceType.SOIL_SENSOR == device.type ?
                                    Text(device.lastHumidityValue, style: TextStyle(color: widget.color, fontSize: 26, fontWeight: FontWeight.bold),)
                                    :
                                    Container(
                                      width: 12,
                                      height: 12,
                                      decoration: BoxDecoration(shape: BoxShape.circle, color: device.status == DeviceStatus.ON ? AppTheme.flatUiEmerald : AppTheme.light_grey ),
                                    )
                                ]),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 20, left: 16, right: 16),
                              child:
                              device.name != null ?
                              Text(device.name,
                                style: TextStyle(
                                  fontFamily:
                                  AppTheme.fontName,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  letterSpacing: 0.2,
                                  color: widget.color,
                                ),
                              ) :
                              Text(device.type.toString(),
                                style: TextStyle(
                                  fontFamily:
                                  AppTheme.fontName,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  letterSpacing: 0.2,
                                  color: widget.color,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 5, left: 16, right: 16),
                              child:
                              Text(device.status == DeviceStatus.ON ? 'On' : 'Off',
                                style: TextStyle(
                                  fontFamily:
                                  AppTheme.fontName,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  letterSpacing: 0.2,
                                  color: AppTheme.light_grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

Widget customDialog(BuildContext context, Color color, Function action) {
  final double avatarRadius = 30.0;
  final double padding = 5.0;

  return Dialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(padding),
    ),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: avatarRadius + padding,
            bottom: padding,
            left: padding,
            right: padding,
          ),
          margin: EdgeInsets.only(top: avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                "Unregister device",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 16.0),
              Text(
                "Are you sure you want to unregister a device?",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Cancel"),
                      ),
                      ElevatedButton(
                        //color: AppTheme.flatUiBlue,
                        onPressed: () {
                          action();
                        },
                        child: Text("Delete", style: TextStyle(color: AppTheme.white),),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: padding,
          right: padding,
          child: CircleAvatar(
            backgroundColor: color,
            radius: avatarRadius,
            child: Icon(Icons.delete, color: AppTheme.white,),
          ),
        ),
      ],
    ),
  );
}