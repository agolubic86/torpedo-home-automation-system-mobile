import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/scheduledTask.dart';
import '../providers/scheduledTasks.dart';
import '../providers/days.dart';

import '../widgets/daySelector.dart';

import '../appTheme.dart';
import '../helpers/CommonUtils.dart';

class ScheduledTaskItem extends StatefulWidget {

  const ScheduledTaskItem(
      {Key key, this.animationController, this.animation, this.setSelected, this.color,})
      : super(key: key);

  final AnimationController animationController;
  final Animation<dynamic> animation;
  final Function setSelected;
  final Color color;

  @override
  State<ScheduledTaskItem> createState() => _ScheduledTaskItemState();
}

class _ScheduledTaskItemState extends State<ScheduledTaskItem> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final scheduledTask = Provider.of<ScheduledTask>(context);
    final scheduledTasks = Provider.of<ScheduledTasks>(context);

    return scheduledTask == null ? SizedBox() : AnimatedBuilder(
      animation: widget.animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - widget.animation.value), 0.0),
            child: Dismissible(
              key: Key(UniqueKey().toString()),
              direction: DismissDirection.endToStart,
              background: Container(),
              secondaryBackground: Container(
                  decoration: BoxDecoration(
                  color: AppTheme.flatUiRed,
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(right: 20),
                    child: Icon(Icons.delete_outline_sharp, color: AppTheme.white, size: 35),
                  ),
                ),
              onDismissed: (direction) {
                scheduledTasks.deleteScheduledTask(scheduledTask.name);
              },
              child: Container(
                    decoration: BoxDecoration(
                      color: AppTheme.white,
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: AppTheme.grey.withOpacity(0.4),
                            offset: const Offset(1.1, 1.1),
                            blurRadius: 10.0),
                      ],
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20, left: 16, right: 16),
                                    child:
                                    Text(scheduledTask.name,
                                      style: TextStyle(
                                        fontFamily:
                                        AppTheme.fontName,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        letterSpacing: 0.2,
                                        color: AppTheme.flatUiBlue,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 10, left: 16, right: 16),
                                    child:
                                    Text('From: ${scheduledTask.startTime} To: ${scheduledTask.endTime}',
                                      style: TextStyle(
                                        fontFamily:
                                        AppTheme.fontName,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12,
                                        letterSpacing: 0.2,
                                        color: AppTheme.light_grey,
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16, right: 16),
                                    child:
                                    Container(
                                      width: 12,
                                      height: 12,
                                      decoration: BoxDecoration(shape: BoxShape.circle, color: scheduledTask.isActive == false ? AppTheme.flatUiEmerald : AppTheme.light_grey ),
                                    )
                                  ),
                                ],
                              ),
                            ],
                          ),

                          Container(
                            padding: EdgeInsets.only(top: 35),
                            height: 150,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                DaySelector(onSelect: ()=>{}, selectable: false, selectedDays: scheduledTask.days.map((dayName) => Days.getByName(dayName)).toList(),),

                                Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.only(top: 15, left:20, right: 20),
                                  child: Text('Devices 1',
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontFamily:
                                      AppTheme.fontName,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      letterSpacing: 0.2,
                                      color: AppTheme.light_grey,
                                    ),
                                    ),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
            ),
          ),
        );
      },
    );
  }
}