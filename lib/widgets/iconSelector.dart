import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


import '../appTheme.dart';

import '../providers/appIcon.dart';
import '../providers/appIcons.dart';

class IconSelector extends StatelessWidget {
  final Function onSelect;


  IconSelector(this.onSelect);

  void setRemoveAllSelection(BuildContext context,List<AppIcon> icons, String selectedIcon) {
    icons.forEach((AppIcon icon) {
      icon.isSelected = false;
      if (selectedIcon == icon.name) {
        icon.isSelected = true;
      }
    });

    Provider.of<AppIcons>(context, listen: false).updateIcons(icons);
  }

  @override
  Widget build(BuildContext context) {
    print("Build icons selector");
    final appIconsData = Provider.of<AppIcons>(context, listen: false);
    final appIcons =  appIconsData.icons;

    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0, left: 16.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: appIcons.map((appIcon) {
                return InkWell(
                  onTap: () {
                    onSelect(appIcon.name);
                    setRemoveAllSelection(context, appIcons, appIcon.name);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: <Widget>[
                        SizedBox(
                          width: 40,
                          height: 40,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                border: Border.all(color: AppTheme.light_grey, width: 1, style: BorderStyle.solid)
                            ),
                            child: appIcon.icon,
                          ),
                        ),
                        if(appIcon.isSelected) Positioned(
                          top: -5,
                          right: -5,
                          child: Container(
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(shape: BoxShape.circle, color: AppTheme.white),
                            child: Container(
                              child: Icon(Icons.check_circle_outline, color: AppTheme.flatUiEmerald, size: 20,),
                              decoration: BoxDecoration(shape: BoxShape.circle,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }).toList()
          ),
        ),
    );
  }
}
