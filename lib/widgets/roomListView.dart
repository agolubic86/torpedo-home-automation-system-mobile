import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/roomItem.dart';

import '../providers/room.dart';
import '../providers/rooms.dart';


class RoomListView extends StatefulWidget {

  RoomListView({Key key, this.mainScreenAnimationController, this.mainScreenAnimation}) : super(key: key);

  final AnimationController mainScreenAnimationController;
  final Animation<dynamic> mainScreenAnimation;

  @override
  _RoomListViewState createState() => _RoomListViewState();
}

class _RoomListViewState extends State<RoomListView> with TickerProviderStateMixin {

  AnimationController animationController;

  Future<Null> _refreshRooms(BuildContext context) async {
    await Provider.of<Rooms>(context, listen: false).getAndSetRooms();
  }

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  void setRemoveAllSelection(String selectedRoomId) {
    List<Room> rooms = Provider.of<Rooms>(context, listen: false).rooms;
    rooms.forEach((Room room) {
      room.isSelected = false;
      if (selectedRoomId == room.id) {
        room.isSelected = true;
      }
    });

    Provider.of<Rooms>(context, listen: false).updateRooms(rooms);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<Rooms>(context, listen: false).getAndSetRooms(),
      builder: (ctx, dataSnapshot) {
        if (dataSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          if (dataSnapshot.error != null) {
            return Center(
              child: Text('Error loading rooms!'),
            );
          } else {
            return SizedBox(
                  height: 215,
                  child: RefreshIndicator(
                onRefresh: () => _refreshRooms(context),
                child: SingleChildScrollView(
                    child: Consumer<Rooms>(
                      builder: (ctx, roomData, child) => AnimatedBuilder(
                        animation: widget.mainScreenAnimationController,
                        builder: (BuildContext context, Widget child) {
                          return FadeTransition(
                            opacity: widget.mainScreenAnimation,
                            child: Transform(
                              transform: Matrix4.translationValues(
                                  0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
                              child: Container(
                                height: 216,
                                width: double.infinity,
                                child: ListView.builder(
                                  padding: const EdgeInsets.only(
                                      top: 0, bottom: 0, right: 16, left: 16),
                                  itemCount: roomData.rooms.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (BuildContext context, int index) {
                                    final int count = roomData.rooms.length > 10 ? 10 : roomData.rooms.length;
                                    final Animation<double> animation =
                                    Tween<double>(begin: 0.0, end: 1.0).animate(
                                        CurvedAnimation(
                                            parent: animationController,
                                            curve: Interval((1 / count) * index, 1.0,
                                                curve: Curves.fastOutSlowIn)));
                                    animationController.forward();

                                    return ChangeNotifierProvider.value(
                                      value: roomData.rooms[index],
                                      child: RoomItem(
                                        animation: animation,
                                        animationController: animationController,
                                        setSelected: setRemoveAllSelection,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                    ),
                    ),
                  ),
                  ),
            );
          }
        }
      },
    );
  }
}
