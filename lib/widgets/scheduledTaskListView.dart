import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/scheduledTaskItem.dart';

import '../providers/scheduledTask.dart';
import '../providers/scheduledTasks.dart';
import '../providers/days.dart';

import '../appTheme.dart';


class ScheduledTaskListView extends StatefulWidget {

  ScheduledTaskListView({Key key, this.mainScreenAnimationController, this.mainScreenAnimation}) : super(key: key);

  final AnimationController mainScreenAnimationController;
  final Animation<dynamic> mainScreenAnimation;

  @override
  _ScheduledTaskListViewState createState() => _ScheduledTaskListViewState();
}

class _ScheduledTaskListViewState extends State<ScheduledTaskListView> with TickerProviderStateMixin {

  AnimationController animationController;

  var _isInit = true;

  Future<Null> _refreshScheduledTasks(BuildContext context) async {
    await Provider.of<ScheduledTasks>(context, listen: false).getAndSetScheduledTasks();
  }

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    if(_isInit) {
      Provider.of<Days>(context).setDays();
    }

    setState(() {
      _isInit = false;
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return FutureBuilder(
      future: Provider.of<ScheduledTasks>(context, listen: false).getAndSetScheduledTasks(),
      builder: (ctx, dataSnapshot) {
        if (dataSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          if (dataSnapshot.error != null) {
            return Center(
              child: Text('Error loading scheduler actions!'),
            );
          } else {
            return SizedBox(
                  height: deviceSize.height,
                  child: RefreshIndicator(
                    notificationPredicate: (ScrollNotification notification) {
                      return notification.depth == 1;
                    },
                    onRefresh: () => _refreshScheduledTasks(context),
                      child: SingleChildScrollView(
                        child: Consumer<ScheduledTasks>(
                          builder: (ctx, scheduledTaskData, child) => AnimatedBuilder(
                            animation: widget.mainScreenAnimationController,
                            builder: (BuildContext context, Widget child) {
                              return FadeTransition(
                                opacity: widget.mainScreenAnimation,
                                child: Transform(
                                  transform: Matrix4.translationValues(
                                      0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
                                  child: Container(
                                    height: deviceSize.height-200,
                                    width: double.infinity,
                                    child: ListView.builder(
                                      padding: const EdgeInsets.only(
                                          top: 20, bottom: 0, right: 16, left: 16),
                                      itemCount: scheduledTaskData.scheduledTasks.length,
                                      scrollDirection: Axis.vertical,
                                      itemBuilder: (BuildContext context, int index) {
                                        final int count = scheduledTaskData.scheduledTasks.length > 10 ? 10 : scheduledTaskData.scheduledTasks.length;
                                        final Animation<double> animation =
                                        Tween<double>(begin: 0.0, end: 1.0).animate(
                                            CurvedAnimation(
                                                parent: animationController,
                                                curve: Interval((1 / count) * index, 1.0,
                                                    curve: Curves.fastOutSlowIn)));
                                        animationController.forward();

                                        return ChangeNotifierProvider.value(
                                          value: scheduledTaskData.scheduledTasks[index],
                                          child:  Padding(
                                                padding: const EdgeInsets.only(bottom:20),
                                                child: ScheduledTaskItem(
                                                  animation: animation,
                                                  animationController: animationController,
                                              ),
                                            ),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                        ),
                      ),
                    ),
                  ),
            );
          }
        }
      },
    );
  }
}
