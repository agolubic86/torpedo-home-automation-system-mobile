import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import '../appTheme.dart';

class LineChartView extends StatelessWidget {

  final Map<String, dynamic> chartData;

  LineChartView(this.chartData);

  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  var selectedButton="1h";

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: AppTheme.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 8,
            color: Colors.black26,
            offset: Offset(0, 2),
          )
        ],
      ),
      child:Padding(
        padding: EdgeInsets.only(left: 15, top: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
                height: deviceSize.height - 500,
                child: LineChart( mainData())
            ),
        ],),
      )
      );
  }

  LineChartData mainData() {
    return LineChartData(
      clipData: FlClipData.all(),
      backgroundColor: Colors.white,
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          if(value%10 == 0)
          return FlLine(
            color: AppTheme.light_grey,
            strokeWidth: 1.0,
          );

          return FlLine(
            color: AppTheme.white,
            strokeWidth: 0.0,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Colors.white,
            strokeWidth: 0.0,
          );
        },
      ),
      /*titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: false,
          reservedSize: 22,
          textStyle:
          const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return '10:30';
              case 1:
                return '11:00';
              case 2:
                return '11:30';
              case 3:
                return '12:00';
              case 4:
                return '12:30';
              case 5:
                return '13:00';
            }
            return value.toString();
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            if(value%10 == 0)
              return value.toString();
            return '';
          },
          reservedSize: 25,
          margin: 10,
        ),
      ),*/
      borderData:
        FlBorderData(show: true, border: Border(left: BorderSide(color: AppTheme.nearlyBlack, width: 1, style: BorderStyle.solid), bottom: BorderSide(color: AppTheme.nearlyBlack, width: 1, style: BorderStyle.solid),)),
      minX: chartData['minX'],
      maxX: chartData['maxX'],
      minY: chartData['minY'],
      maxY: chartData['maxY'],
      lineBarsData: [
        LineChartBarData(
          spots: chartData['spots'],
          isCurved: true,
          //colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            //colors: gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}
