import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


import '../appTheme.dart';

import '../providers/days.dart';
import '../providers/day.dart';

class DaySelector extends StatelessWidget {
  final Function onSelect;
  final bool selectable;
  final List<Day> selectedDays;

  DaySelector({this.onSelect, this.selectable=true, this.selectedDays});

  @override
  Widget build(BuildContext context) {
    final daysData = Provider.of<Days>(context, listen: false);
    final days = daysData.days;

    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0, left: 16.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: days.map((day) {
                return InkWell(
                  splashColor: selectable ? Colors.grey : Colors.transparent,
                  onTap: () {
                    if(selectable) {
                      onSelect(day);
                      day.isSelected = !day.isSelected;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: <Widget>[
                        SizedBox(
                          width: 40,
                          height: 40,
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                border: Border.all(color: AppTheme.light_grey, width: 1, style: BorderStyle.solid)
                            ),
                            child: Text(day.displayName, textAlign: TextAlign.center , style: TextStyle(color: AppTheme.flatUiBlue, fontWeight: FontWeight.bold),),
                          ),
                        ),
                        if(day.isSelected || (selectedDays !=null && selectedDays.contains(day))) Positioned(
                          top: -5,
                          right: -5,
                          child: Container(
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(shape: BoxShape.circle, color: AppTheme.white),
                            child: Container(
                              child: Icon(Icons.check_circle_outline, color: AppTheme.flatUiEmerald, size: 20,),
                              decoration: BoxDecoration(shape: BoxShape.circle,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }).toList()
          ),
        ),
    );
  }
}
