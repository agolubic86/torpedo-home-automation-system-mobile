import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../appTheme.dart';
import '../helpers/CommonUtils.dart';

import '../widgets/deviceItem.dart';

import '../providers/device.dart';
import '../providers/devices.dart';
import '../providers/room.dart';
import '../providers/rooms.dart';

class DeviceListView extends StatefulWidget {
  const DeviceListView(
      {Key key, this.mainScreenAnimationController, this.mainScreenAnimation, this.showAll = false})
      : super(key: key);

  final AnimationController mainScreenAnimationController;
  final Animation<dynamic> mainScreenAnimation;
  final bool showAll;

  @override
  _DeviceListViewState createState() => _DeviceListViewState();
}

class _DeviceListViewState extends State<DeviceListView>
    with TickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Room> rooms = Provider.of<Rooms>(context).rooms;
    Room selectedRoom;

    if(rooms.isNotEmpty) {
      for (var room in rooms) {
        if (room.isSelected) {
          selectedRoom = room;
        }
      }

      if(selectedRoom == null) {
        selectedRoom = rooms[0];
      }

    }

    return FutureBuilder(
      future: Provider.of<Devices>(context, listen: false).findAllDevices(),
      builder: (ctx, dataSnapshot) {
        if (dataSnapshot.connectionState == ConnectionState.waiting) {
          animationController.reset();
          return Center(child: CircularProgressIndicator());
        } else {
          if(dataSnapshot.error != null) {
            return Center(
              child: Text('Error loading devices'),
            );
          } else {
            return AnimatedBuilder(
                  animation: widget.mainScreenAnimationController,
                  builder: (BuildContext context, Widget child) {
                    return FadeTransition(
                      opacity: widget.mainScreenAnimation,
                      child: Transform(
                        transform: Matrix4.translationValues(
                            0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
                        child: AspectRatio(
                          aspectRatio: widget.showAll ? 0.6 : 1.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8),
                            child: Consumer<Devices> (
                            builder: (ctx, deviceData, child) {
                              if(selectedRoom == null) {
                                return SizedBox();
                              }
                              List<Device> devices = widget.showAll ? deviceData.devices : deviceData.findByRoomId(selectedRoom.id);
                              return GridView(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 16, bottom: 16),
                                physics: const BouncingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                children: List<Widget>.generate(
                                  devices.length,
                                      (int index) {
                                    final int count = devices.length;
                                    final Animation<double> animation =
                                    Tween<double>(begin: 0.0, end: 1.0).animate(
                                      CurvedAnimation(
                                        parent: animationController,
                                        curve: Interval(
                                            (1 / count) * index, 1.0,
                                            curve: Curves.fastOutSlowIn),
                                      ),
                                    );
                                    animationController.forward();
                                    return ChangeNotifierProvider.value(
                                      value: devices[index],
                                      child: DeviceItem(
                                        animation: animation,
                                        animationController: animationController,
                                        color: widget.showAll ? AppTheme.flatUiBlue : HexColor(selectedRoom.color),
                                      ),
                                    );
                                  },
                                ),
                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  mainAxisSpacing: 24.0,
                                  crossAxisSpacing: 24.0,
                                  childAspectRatio: 1.2,
                                ),
                              );

                            }
                            )
                          ),
                        ),
                      ),
                    );
                  },
                );
          }
        }

      }
    );
  }
}
