import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:torpedo_home_automation_system/helpers/TimeOfTheDayConverter.dart';

import '../widgets/daySelector.dart';
import '../widgets/deviceSelector.dart';

import '../providers/days.dart';
import '../providers/day.dart';
import '../providers/scheduledTask.dart';
import '../providers/scheduledTasks.dart';
import '../providers/device.dart';

import '../appTheme.dart';

class AddScheduledTask extends StatefulWidget {

  const AddScheduledTask() : super();

  @override
  State<AddScheduledTask> createState() => _AddScheduledTaskState();
}

class _AddScheduledTaskState extends State<AddScheduledTask> with TickerProviderStateMixin {

  //animation
  AnimationController animationController;
  Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  final TimeOfDay selectedTime = TimeOfDay.now();

  // input fields
  List<Day> selectedDays = [];
  Device selectedDevice;
  List<Device> selectedDevices = [];
  final taskNameController = TextEditingController();
  final turnOnController = TextEditingController();
  final turnOffController = TextEditingController();

  var _isInit = true;
  var _isLoading = false;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setAnimationData();
  }


  @override
  void dispose() {
    animationController.dispose();
    turnOnController.dispose();
    turnOffController.dispose();
    taskNameController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    if(_isInit) {
      Provider.of<Days>(context).setDays();
    }

    setState(() {
      _isInit = false;
    });

    super.didChangeDependencies();
  }

  Future<void> setAnimationData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 400));
    setState(() {
      opacity2 = 1.0;
    });
  }

  void onDaySelect(Day day) {
    setState(() {
      selectedDays.add(day);
    });
  }

  void onDeviceSelect(Device device) {
    setState(() {
      selectedDevices.add(device);
      selectedDevice = device;
    });
  }

  Future<void> addNewScheduledTask() async {
    setState(() {
      _isLoading = true;
    });

    String taskName = taskNameController.text.isEmpty ? "" : taskNameController.text;
    ScheduledTask newScheduledTask = new ScheduledTask(
        name: taskName,
        days: selectedDays.map((day) => day.index).toList(),
        startTime: turnOnController.text,
        endTime: turnOffController.text,
        device: selectedDevice
    );
    if(newScheduledTask.isValid()) {
      await Future<dynamic>.delayed(const Duration(milliseconds: 500));
      Provider.of<ScheduledTasks>(context, listen: false).addScheduledTask(newScheduledTask).then((_) => {
        setState(() {
          _isLoading = false;
        })
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final deviceOrientation = MediaQuery.of(context).orientation;

    return Container(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(width: deviceSize.width,)
            ],
          ),
          Positioned(
            top: 60,
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                color: AppTheme.nearlyWhite,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(32.0),
                    topRight: Radius.circular(32.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppTheme.grey.withOpacity(0.2),
                      offset: const Offset(1.1, 1.1),
                      blurRadius: 10.0),
                ],
              ),
              child: Stack( children: <Widget>[
                Positioned(
                  top: 15,
                  left: deviceSize.width / 2 - 60,
                  child: SizedBox(
                    width: 120,
                    height: 7,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        color: AppTheme.light_grey,

                      ),),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 10, left: 8, right: 8),
                    child: Container(
                      constraints: BoxConstraints(
                        minHeight: 500,
                        minWidth: deviceOrientation == Orientation.portrait ? deviceSize.height : deviceSize.width
                      ),
                      child: Stack(
                        children: <Widget>[

                          // Buttons and Title
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: opacity1,
                            child: Container(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  TextButton(
                                    onPressed: () { Navigator.of(context).pop(); },
                                    child: Text(
                                      'CANCEL',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                        letterSpacing: 0.27,
                                        color: AppTheme.light_grey,
                                      ),
                                    ),),
                                  Text(
                                    'Add new task',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      letterSpacing: 0.27,
                                      color: AppTheme.darkerText,
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      addNewScheduledTask().then((_) => {
                                        Navigator.of(context).pop()
                                      });
                                    },
                                    child: Text(
                                      'DONE',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                        letterSpacing: 0.27,
                                        color: AppTheme.flatUiBlue,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          // Days select
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: opacity2,
                            child: Container(
                              margin: EdgeInsets.only(top: 50.0),
                              child: SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Divider(
                                      color: AppTheme.light_grey,
                                    ),

                                    getSectionTitle('Enter task name'),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                                      child: TextField(
                                        controller: taskNameController,
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                                          labelText: 'Task name',
                                        ),
                                      ),
                                    ),

                                    // Day Select
                                    getSectionTitle("Select days"),
                                    DaySelector(onSelect: onDaySelect),

                                    // Time pickers
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [

                                          // Time picker on
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              getSectionTitle('Turn on'),
                                              Padding(
                                                padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                                                child: Container(
                                                  width: 100,
                                                  child: TextField(
                                                    controller: turnOnController,
                                                    onTap: () {
                                                      _selectTime(context, turnOnController);
                                                    },
                                                    readOnly: true,
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(10),
                                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                                                      labelText: '',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),

                                          // Time picker off
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              getSectionTitle('Turn off'),
                                              Padding(
                                                padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                                                child: Container(
                                                  width: 100,
                                                  child: TextField(
                                                    controller: turnOffController,
                                                    onTap: () {
                                                      _selectTime(context, turnOffController);
                                                    },
                                                    readOnly: true,
                                                    decoration: InputDecoration(
                                                      contentPadding: EdgeInsets.all(10),
                                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                                                      labelText: '',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),



                                    getSectionTitle('Add devices'),
                                    DeviceSelector(onSelect: onDeviceSelect),

                                  ]
                                ),
                            ),
                            ),
                          ),
                        ],
                      ),
                    ),
                ),
              ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getSectionTitle(final String title) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 16, right: 16, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              letterSpacing: 0.27,
              color: AppTheme.darkerText,
            ),
          ),
        ],
      ),
    );
  }


  Future<TimeOfDay> _selectTime(BuildContext context, TextEditingController controller) async {
    final selected = await showTimePicker(
      context: context,
      initialTime: selectedTime,
      builder: (context, childWidget) {
        return MediaQuery(
            data: MediaQuery.of(context).copyWith(
              // Using 24-Hour format
                alwaysUse24HourFormat: true),
            // If you want 12-Hour format, just change alwaysUse24HourFormat to false or remove all the builder argument
            child: childWidget
        );
      }
    );

    if (selected != null) {
      controller.text = selected.to24hours();
      return selected;
    }
    return selectedTime;
  }

}
